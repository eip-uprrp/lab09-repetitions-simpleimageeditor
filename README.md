#Lab. 9: Estructuras de repetición 2- Simple Image Editor

<img src="http://i.imgur.com/8mEUiNZ.png?1" width="215" height="174">  <img src="http://i.imgur.com/8oYmwg7.png?1" width="215" height="174">  <img src="http://i.imgur.com/GJfSRTY.png?1" width="215" height="174">


<p> </p>

Una de las ventajas de utilizar programas de computadoras es que podemos realizar tareas repetitivas fácilmente. Los ciclos como `for`, `while`, y `do-while` son  estructuras de control que nos permiten repetir un conjunto de instrucciones. A estas estructuras también se les llama *estructuras de repetición*. En la experiencia de laboratorio de hoy diseñarás e implementarás algoritmos simples de procesamiento de imágenes para practicar el uso de ciclos anidados en la manipulación de arreglos bi-dimensionales. 

##Objetivos:

1. Aplicar ciclos anidados para implementar algoritmos simples de procesamiento de imágenes.

2. Utilizar expresiones aritméticas para transformar colores de pixeles.

3. Accesar pixeles en una imagen y  descomponerlos en sus componentes rojo, azul y verde.


##Pre-Lab:

Antes de llegar al laboratorio debes:

1. conseguir y tener disponible uno o más archivos con una imagen a color en alguno de los siguientes formatos: `tiff, jpg, png`.

2. haber repasado los conceptos básicos relacionados a estructuras de repetición y ciclos anidados.

3. conocer las funciones básicas de `QImage` para manipular los pixeles de las imágenes 

4. haber estudiado los conceptos e instrucciones de la sesión de laboratorio.

5. haber tomado el [quiz Pre-Lab 9](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=7576) (recuerda que el quiz Pre-Lab puede tener conceptos explicados en las instrucciones del laboratorio).



##Edición de imágenes

En esta experiencia de laboratorio, trabajarás con varios conceptos y destrezas básicas de edición de imágenes. Te proveemos un interfase gráfico (GUI) simple que le permite al usuario cargar una imagen e invertirla vertical y horizontalmente.  Tu tarea es crear e implementar una función para convertir una imagen a color a una imagen con tonos de gris, y otra función que convierta una imagen a color a una imagen en blanco y negro.



###Biblioteca

El código que te proveemos continene los objetos:

* `origImage`   // contiene la información de la imagen original que vas a editar
*  `editImage`  // guardará la imagen editada

Las imágenes se pueden manejar en Qt mediante la clase `QImage`. Los objetos de clase `QImage` tienen algunos métodos que serán útilies para esta experiencia de laboratorio:


* `width()`      // devuelve el valor entero del ancho de la imagen
* `height()`      // devuelve el valor entero de la altura de la imagen
* `pixel(i, j)`       // devuelve el QRgb del pixel en la posición `(i,j)`
* `setPixel(i,j, pixel)`   // modifica el valor del pixel en la posición `(i, j)` al valor pixel QRgb,

Cada pixel de una imagen se puede manipular como un objeto de clase `QRgb`. Las siguientes funciones son útiles para trabajar con datos de clase `QRgb`:



* `qRed(pixel)`   // devuelve el tono del color rojo del pixel
* `qGreen(pixel)` // devuelve el tono del color verde del pixel
* `qBlue(pixel)`  // devuelve el tono del color azul del pixel
* `qRgb(int red, int green, int blue)` // construye un pixel `QRgb` compuesto de los valores de rojo, verde y azul recibidos.


####Ejemplos:

1. `QRgb myRgb = qRgb(0xff, 0x0, 0xff);`: Asigna al objeto `myRgb` el valor `0xff00ff` que representa el color <img src="http://i.imgur.com/CQeVEHn.png?1" width="50" height="25">
2. Si la siguiente imagen `4 x 4` de pixeles representa el objeto `origImage`,

  <div align='center'><img src="http://i.imgur.com/8oYmwg7.png?1"></div>

  entonces `origImage.pixel(2,1)` captura un valor `rgb` que representa el color azul (`0x0000ff`).

3. La siguiente instrucción asigna el color rojo al pixel en posición `(2,3)` en la imagen editada:

  `editImage.setPixel(2,3,qRgb(0xff,0x0,0x00));`.
4. La siguiente instrucción le asigna a `greenContent` el valor del tono de verde que contiene el pixel `(1,1)` de  `origImage`:

  `int greenContent = qGreen(origImage.pixel(1,1));`.


##Sesión de laboratorio:

**Instrucciones**

1. Abre un terminal y escribe el comando `git clone https://bitbucket.org/eip-uprrp/lab09-repetitions-simpleimageeditor.git` para descargar la carpeta `Lab09-Repetitions-SimpleImageEditor` a tu computadora.


2.  Haz doble "click" en el archivo `SimpleImageEditor.pro` para cargar este proyecto a Qt.   

3. El archivo `filter.cpp` es donde estarás añadiendo código. 

###Ejercicio 0: Entendiendo el código provisto

El código que te proveemos crea el interfase de la Figura 1. 


<div align='center'><img src="http://i.imgur.com/X3bqP6C.png" width="600" height="400" alt="New Project" /></div>

<div align='center'><b>Figura 1.</b> Interfase del editor de imágenes.</div>

<p></p>

En el archivo `mainwindow.cpp`, las etiquetas `lblOriginalImage` y `lblEditedImage` corresponden a las partes de la interfase que identifican  la imagen original y la imagen procesada. Los botones

* `btnLoadImage`
* `btnSaveImage`
* `btnInvertThreshold`
* `btnFlipImageHorizontally`
* `btnFlipImageVertically`
* `btnGreyScaleFilter`
* `btnRevertImage` 

están conectados a  funciones de modo que cuando se presione el botón de la interfase se haga alguna tarea. Por ejemplo, cuando se presiona `btnLoadImage`, saldrá una ventana para seleccionar el archivo con la imagen para editar, al seleccionar el archivo, se lee y se asigna la imagen al objeto `originalImage`. El deslizador `thresholdSlider` puede asumir valores entre 0 y 255.

En los ejercicios siguientes estarás usando mayormente los objetos `originalImage` y `editedImage` de la clase `QImage`. 

Estudia la función `HorizontalFlip` del archivo `filter.cpp` para que entiendas su operación. ¿Cuál crees que es el propósito del objeto `pixel`?

Compila y corre el programa. Prueba los botones `Load New Image` y `Flip Image Horizontally` con las imágenes que trajiste para que valides las operaciones de los botones.

###Ejercicio 1: Imagen con tonos de gris

El "image grayscale" es una operación que se puede usar para convertir una imagen a color a una imagen que solo tenga tonalidades de gris. Para hacer esta conversión se usa la siguiente fórmula en cada uno de los pixeles:

`gray = (red * 11 + green * 16 + blue * 5)/32 ;`

donde "red", "green" y "blue" son los valores para los colores rojo, verde y azul en el pixel de la imagen original a color, y "gray" será el color asignado a los colores rojo, verde y azul en el pixel de la imagen editada. Esto es,

`editedImage.setPixel( i, j, qRgb(gray, gray, gray) )`.

1. Utilizando pseudocódigo, expresa el algoritmo para convertir una imagen a color a una imagen solo con tonalidades de gris. El apéndice de este documento contiene algunos consejos sobre buenas prácticas al hacer pseudocódigos.
2. Completa la función `GreyScale` en el archivo `filter.cpp` para implementar el algoritmo de tonalidades de gris.


<img src="http://i.imgur.com/8mEUiNZ.png?1" width="215" height="174">  <img src="http://i.imgur.com/7NBNoBF.png?1" width="215" height="174">  

<p> </p>

###Ejercicio 2: Imagen en blanco y negro ("Thresholding")

"Thresholding" es una operación que se puede utilizar para convertir una imagen a color a una imagen en blanco y negro. Para hacer esta conversión debemos decidir cuáles colores de la imagen original van a convertirse en pixeles blancos y cuáles serán negros. Una manera sencilla de decidir esto es computando el promedio de los componentes rojo, verde y azul de cada pixel. Si el promedio es menor que el valor umbral ("threshold"), entonces cambiamos el pixel a negro; de lo contrario se cambia a blanco.

1. Utilizando pseudocódigo, expresa el algoritmo para "thresholding". Presume que puedes leer el valor umbral del deslizador.
2. En el programa, si la caja `chkboxThreshold` está marcada, se hace una invocación a la función `applyThresholdFilter`. La función `applyThresholdFilter` también es invocada cada vez que se cambia el valor del deslizador.
3. Completa la función `ThresholdFilter` de modo que implemente el algoritmo de "threshold" en la imagen a color utilizando el valor del deslizador como umbral. Si se implementa correctamente, la imagen de la derecha debe ser la imagen original pero en blanco y negro. El valor umbral es un parámetro de la función `ThresholdFilter`. 
4. El parámetro booleano `color` tendrá el valor `true` si la opción de invertir los colores fue seleccionada. Escribe código de modo que los colores blanco y negro se inviertan en la imagen si `color` asume el valor `true`.
5. Prueba tu programa con distintas imeagenes y distintos valores de umbral.


<img src="http://i.imgur.com/8mEUiNZ.png?1" width="215" height="174">  <img src="http://i.imgur.com/GJfSRTY.png?1" width="215" height="174">  

<p> </p>

## Entrega:

Entrega el archivo `filter.cpp` utilizando el [enlace en Moodle](http://moodle.ccom.uprrp.edu/mod/assignment/view.php?id=7578).

##Apéndice: Buenas prácticas al escribir pseudocódigos

1. Provee una descripción de los dartos de entrada y salida
2. Enumera los pasos
3. Usa estructuras de repetición comunes: `if, else, for, while`
4. Indenta los bloques de pasos que están dentro de una estructura de repetición o decisión, "Python-style"
5. No necesitas declarar los tipos de las variables pero si debes inicializarlas. Esto es especialmente importante para contadores y acumuladores
6. Recuerda que el propósito de un pseudocódigo es que un humano lo pueda entender.

Ejemplo:
```
Input: n, a positive integer
Output: true if n is prime, false otherwise
---------------------------------------------------------
1. for i = 3 to n / 2
2.   if n % i == 0:
3.      return false
4. return true
```
