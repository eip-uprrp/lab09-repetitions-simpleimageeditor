#include "mainwindow.h"
#include <QImage>
#define WHITE 0xffffff
#define BLACK 0

///
/// Function that applies a greyscale filter to the edited image.
/// It works by turning each pixel into a tone of grey.
///

void MainWindow::GrayScale(QImage &originalImage, QImage &editedImage){
    // Space to implement the grayscale filter.
    
    
    
   }

///
/// Function that applies a vertical flip to the edited image.
/// For this we use two for loops to access the pixels of the images.
/// In the first loop we go through the x axis and in the second we go
/// through the y axis and inside of it we put the pixel from the original
/// image in the heigth-1-j position of the edited image.
///
void MainWindow::VerticalFlip(QImage originalImage, QImage &editedImage){
    // Space to implement the vertical flip filter.
    int width = editedImage.width();
    int height = editedImage.height();
    QRgb pixel;
    for(int i=0; i<width; i++){
        for(int j=0; j<height; j++){
            pixel = originalImage.pixel(i,j);
            editedImage.setPixel(i,height-1-j,pixel);
        }
    }
}

///
/// Function that applies a horizontal flip to the edited image
/// For this we use two for loops to access the pixels of the images.
/// In the first loop we go through the x axis and in the second we go
/// through the y axis and inside of it we put the pixel from the original
/// image in the width-1-i position of the edited image.
///
void MainWindow::HorizontalFlip(QImage originalImage, QImage &editedImage){
    // Space to implement the horizontal flip filter.
    int width = editedImage.width();
    int height = editedImage.height();
    QRgb pixel;
    for(int i=0; i<width; i++)
        for(int j=0; j<height; j++){
            pixel = originalImage.pixel(i,j);
            editedImage.setPixel(width-1-i,j,pixel);
        }
}

///
/// Function that applies a threshold filter to the edited image.
///

void MainWindow::ThresholdFilter(QImage &originalImage, QImage &editedImage,int threshold, bool color){
    // Space to implement the Threshold Filter.
    // threshold contains the treshold value
    // color is a variable to flip from Black to White for pixel
    // over the threshold and viceversa.
    
    
    
   }
