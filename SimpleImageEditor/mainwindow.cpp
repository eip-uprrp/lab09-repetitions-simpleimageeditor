#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->thresholdSlider->setDisabled(true);
    ///
    /// KEYBOARD SHORTCUTS
    /// Loads a image with cmd + o
    new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_O), this, SLOT(on_btnLoadImage_clicked()));
    ///
    /// Saves a image with cmd + s
    ///
    new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_S), this, SLOT(on_btnSaveImage_clicked()));
    ///
    /// Closes window with cmd + w
    ///
    new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_W), this, SLOT(close()));
    ///
    /// Inverts threshold color with cmd + t
    ///
    new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_T), this, SLOT(on_btnInvertThreshold_clicked()));
    ///
    /// Flips horizontally a image with cmd + f
    ///
    new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_F), this, SLOT(on_btnFlipImageHorizontally_clicked()));
    ///
    /// Flips vertically a image with cmd + v
    ///
    new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_V), this, SLOT(on_btnFlipImageVertically_clicked()));
    ///
    /// Applies greyscale filter to a image with cmd + g
    ///
    new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_G), this, SLOT(on_btnGreyScaleFilter_clicked()));
    ///
    /// Resets an edited image to the orignal one to a image with cmd + r
    ///
    new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_R), this, SLOT(on_btnRevertImage_clicked()));
    ///
    /// Resets an edited image to the orignal one to a image with cmd + i
    ///
    new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_I), this, SLOT(instructions()));
}

MainWindow::~MainWindow(){
    delete ui;
}

///
/// ACTION FUNCTIONS
///
void MainWindow::on_actionLoad_Image_triggered(){
   on_btnLoadImage_clicked();
}


void MainWindow::on_actionSave_Image_triggered(){
    on_btnSaveImage_clicked();
}

void MainWindow::on_actionClose_Window_triggered(){
    close();
}

void MainWindow::on_actionInvert_Threshold_Colors_triggered(){
    on_btnInvertThreshold_clicked();
}

void MainWindow::on_actionFlip_Image_Horizontally_triggered(){
    on_btnFlipImageHorizontally_clicked();
}

void MainWindow::on_actionFlip_Image_Vertically_triggered(){
    on_btnFlipImageVertically_clicked();
}

void MainWindow::on_actionApply_Grey_Scale_Filter_triggered(){
    on_btnGreyScaleFilter_clicked();
}

void MainWindow::on_actionRevert_Edited_Image_to_Original_triggered(){
    on_btnRevertImage_clicked();
}

void MainWindow::on_actionInstructions_triggered(){
    instructions();
}

///
/// THRESHOLD FILTER FUNCTIONS
/// Function that is called when the threshold checkbox is clicked
///
void MainWindow::on_chboxThreshold_clicked(){
    if (ui->chboxThreshold->isChecked()){
        ui->thresholdSlider->setEnabled(true);
        applyThresholdFilter();
        ui->lblEditedImage->setPixmap(QPixmap::fromImage(editedImage));
    }
    else
        ui->thresholdSlider->setDisabled(true);
}

///
/// Function that applies a threshold filter to a image.
///
void MainWindow::applyThresholdFilter(){
    ThresholdFilter(originalImage, editedImage, ui->thresholdSlider->value(), color) ;
}

///
/// Function called when the value of the threshold slider is changed
///
void MainWindow::on_thresholdSlider_sliderReleased(){
    applyThresholdFilter();
    ui->lblEditedImage->setPixmap(QPixmap::fromImage(editedImage));
}

///
/// Function that inverts the threshold color of an image
///
void MainWindow::on_btnInvertThreshold_clicked(){
    color = !color;
    if(ui->chboxThreshold->isChecked()){
        applyThresholdFilter();
        ui->lblEditedImage->setPixmap(QPixmap::fromImage(editedImage));
    }
}

///
/// PUSH BUTTON FUNCTIONS
/// Function that loads an image to the window
///
void MainWindow::on_btnLoadImage_clicked(){
    QString fname = QFileDialog::getOpenFileName(this, tr("Choose an image"), QDir::homePath());
        if (!fname.isEmpty()){
            QImage image(fname);
            if (image.isNull())
                QMessageBox::information(this, tr("Choose an image"),tr("Cannot load %1.").arg(fname));
            originalImage=image;
            editedImage=image;
        }
    ui->lblOriginalImage->setPixmap(QPixmap::fromImage(originalImage));
    ui->lblEditedImage->setPixmap(QPixmap::fromImage(editedImage));
}

///
/// Function that saves an image to a given path
///
void MainWindow::on_btnSaveImage_clicked()
{
    QPixmap out = QPixmap::grabWidget(this,361,10,481,481);
    QString fname = QFileDialog::getSaveFileName(this, tr("Save Edited Image"), (""), tr("PNG (*.png)" ));
    editedImage.save(fname, "PNG");
}

///
/// Function that flips an image horizontally
///
void MainWindow::on_btnFlipImageHorizontally_clicked(){

    HorizontalFlip(editedImage, editedImage) ;
    ui->lblEditedImage->setPixmap(QPixmap::fromImage(editedImage));
}

///
/// Function that flips an image vertically
///
void MainWindow::on_btnFlipImageVertically_clicked(){

    VerticalFlip(editedImage, editedImage) ;
    ui->lblEditedImage->setPixmap(QPixmap::fromImage(editedImage));
}

///
/// Function that applies a Grey Scale filter to an image
///
void MainWindow::on_btnGreyScaleFilter_clicked(){

    GrayScale(editedImage, editedImage) ;
    ui->lblEditedImage->setPixmap(QPixmap::fromImage(editedImage));
}

///
/// Function that reverts an edited image to its original state
///
void MainWindow::on_btnRevertImage_clicked(){
    editedImage = originalImage;
    ui->lblEditedImage->setPixmap(QPixmap::fromImage(editedImage));
}

///
/// Function that explains how to use the program for users.
///
void MainWindow::instructions(){
    QString h[8];
    h[0]="1. Load an image by pressing the \"Load Image\" button, this can also be done from the edit menu or using a keyboard shortcut.";
    h[1]="\n\n2. To apply a greyscale filter to the image click the \"Greyscale Filter\" button, this can also be done from the edit menu or using a keyboard shortcut.";
    h[2]="\n\n3. To apply a threshold filter to the image click the \"Threshold\" check box. If you wish to invert the black and white colors you can press the \"Invert Threshold Colors\" button, this can also be done from the edit menu or using a keyboard shortcut.";
    h[3]=" You can change the value of the threshold with the slider below the check box.";
    h[4]="\n\n4. To flip the image horizontally click the \"Flip Image Horizontally\" button, this can also be done from the edit menu or using a keyboard shortcut.";
    h[5]="\n\n5. To flip the image vertically click the \"Flip Image Horizontally\" button, this can also be done from the edit menu or using a keyboard shortcut.";
    h[6]="\n\n6. If you feel like you did not create a masterpiece, you can revert the image back to original by clicking the button \"Revert Image to Original\", this can also be done from the edit menu or using a keyboard shortcut.";
    h[7]="\n\n7. When you are satisfied with the results you can save the image as a PNG by clicking the button \" Save edited image\", this can also be done from the edit menu or using a keyboard shortcut.";
    QMessageBox mbox;
    mbox.setWindowTitle("How to use the Simple Image Editor");
    mbox.setText(h[0]+h[1]+h[2]+h[3]+h[4]+h[5]+h[6]+h[7]);
    mbox.exec();
}
