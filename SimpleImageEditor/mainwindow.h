#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QDir>
#include <QMessageBox>
#include <QSlider>
#include <QShortcut>
#include <QMessageBox>

#define WHITE 0xffffff
#define BLACK 0

namespace Ui {
class MainWindow;
}

void GrayScale(QImage &, QImage &) ;
void HorizontalFlip(QImage , QImage &) ;
void VerticalFlip(QImage, QImage &) ;
void ThresholdFilter(QImage &, QImage &, int, bool) ;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void GrayScale(QImage &, QImage &);
    void VerticalFlip(QImage , QImage &);
    void HorizontalFlip(QImage , QImage &);
    void ThresholdFilter(QImage &, QImage &,int , bool);
    ~MainWindow();

private slots:

    // Action Functions
    void on_actionLoad_Image_triggered();                      // Loads image to window
    void on_actionSave_Image_triggered();                      // Saves an image to a given path
    void on_actionClose_Window_triggered();                    // Closes a window
    void on_actionInvert_Threshold_Colors_triggered();         // Inverts the color of the threshold value
    void on_actionFlip_Image_Horizontally_triggered();         // Flips an image horizontally
    void on_actionFlip_Image_Vertically_triggered();           // Flips an image vertically
    void on_actionApply_Grey_Scale_Filter_triggered();         // Applies threshold filter to an image
    void on_actionRevert_Edited_Image_to_Original_triggered(); // Reverts an edited image to its original state
    void on_actionInstructions_triggered();                    // Shows program instructions


    // Applies threshold filter
    void applyThresholdFilter();

    // Shows program instructions.
    void instructions();

    // Changes the value of the threshold
    void on_thresholdSlider_sliderReleased();

    // Loads an image
    void on_btnLoadImage_clicked();

    // Saves an image
    void on_btnSaveImage_clicked();

    // Sets the threshold slider to enabled or disabled
    void on_chboxThreshold_clicked();

    // Inverts the threshold
    void on_btnInvertThreshold_clicked();

    // Flips horizontally an image
    void on_btnFlipImageHorizontally_clicked();

    // Flips vertically an image
    void on_btnFlipImageVertically_clicked();

    // Applies Grey Scale filter to an image
    void on_btnGreyScaleFilter_clicked();

    // Reverts an image to its original state
    void on_btnRevertImage_clicked();

private:
    Ui::MainWindow *ui;
    QImage originalImage;  //This will hold the original image.
    QImage editedImage;    //This will hold the edited image.
    bool color;   //Boolean value of the switch button for the threshold filter.
};

#endif // MAINWINDOW_H
